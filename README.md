# Md Tests

#### Lista
1. Przejść do listy kanałów płatności.
2. Na liście kliknąć na wiersz z kanałem, który chcemy zedytować.
3. Dostępne opcje:
    1. **Status kanału (Status)** – Określa czy kanał jest aktualnie dostępny. Odświeżenie kanałów następuje co 5 minut (w przypadku poprawnego skonfigurowania CRONa).
    1. **Opis (Description)** – Opis wyświetlany klientowi pod nazwą kanału.
    1. **Kolejność (Sort Order)** – Kolejność sortowania na liście kanałów, gdzie:
        1 – pierwsza pozycja na liście,
        2 – druga pozycja na liście,
        ...
       0 – ostatnia pozycja na liście.
    1. (informacyjnie) **Rodzaj (Type)**.


### Lista z kontynuacją i fragmentami kodu
1. Wykonać komendę
```bash
composer update bluepayment-plugin/module-bluepayment
bin/magento setup:upgrade
bin/magento setup:di:compile
bin/magento cache:flush
```

### Przez paczkę .zip
1. Pobrać najnowszą wersję modułu ze strony http://s.bm.pl/wtyczki.
2. Wgrać plik .zip do katalogu głównego Magento.
3. Będąc w katalogu głównym Magento, wykonać komendy:
```bash
unzip -o -d app/code/BlueMedia/BluePayment bm-bluepayment-*.zip && rm bm-bluepayment-*.zip
bin/magento setup:upgrade
bin/magento setup:di:compile
bin/magento cache:flush
```
4. Moduł został zaktualizowany.

### Obrazek
![Diagram sekwencji](Diagram_sekwencji.png)

#### Tabela ze znakami

| Dane | AIS | 1PLN | FOTO |
| --- | --- | --- | --- |
| Specjalne znaki html5 w dedykowanych fontach | ✔️ | 🤓 | ❌ | 
| Znaki w każdym foncie  | ✔ | ✖ | ✖ |
| z użyciem grafiki | ![✔](ico-done-v2.png) | ![✔](ico-done-v2.png) | ![✔](ico-done-v2.png) |

 





To repozytorium jest przeznaczone do testowania dokumentacji MD

## Każdy może poprosić o dostęp do edycji tego repozytorium.

Wytyczne odnośnie tworzenia dokumentacji MD można znaleźć pod jednym z poniższych linków:
 - https://gitlab.com/partnerit-public/md-example - repozytorium dokumentacji
 - https://dev-v2.bluemedia.pl/przykladowa-dokumentacja-md - opublikowana dokumentacja z powyższego repozytorium


## Linki:  
 - Dokumentacja: https://dev-v2.bluemedia.pl/md-tests
 - Repozytorium: https://gitlab.com/partnerit-public/md-tests
